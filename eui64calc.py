#!/usr/bin/env python3
import argparse
import ipaddress
import re
import sys


def parse_args():
    parser = argparse.ArgumentParser(
        add_help=True, description="EUI64 address calculator"
    )

    arguments = parser.add_argument_group("arguments")
    arguments.add_argument("-m", "--mac", required=True, help="mac address")
    arguments.add_argument("-p", "--prefix", required=True, help="prefix")

    return parser.parse_args()


def validate_mac(mac):
    if re.match("[0-9a-fA-F]{2}([-:]?)[0-9a-fA-F]{2}(\\1[0-9a-fA-F]{2}){4}$", mac):
        return mac.lower()
    else:
        sys.exit("Invalid MAC-address.")


def validate_prefix(prefix):
    try:
        if not "::" in prefix and prefix.count(":") < 7:
            if prefix.endswith(":"):
                prefix += ":"
            else:
                prefix += "::"
        ipaddr = ipaddress.ip_address(re.sub("/[0-9]+$", "", prefix))
        return ipaddr
    except ValueError as e:
        sys.exit("Error: {}".format(e))


def calculate_eui64addr(mac, prefix):
    """
        The eui-64 ipv6 address is calculated by taking the OUI / Vendor #
        (organizationally unique identifier) of the mac addres and converting
        the first octet to binary. The 7th bit of the binary output is flipped,
        so 1 becomes 0 and 0 becomes one and this is converted back to hex.
        
        The output is then appended by "ff:fe" and the UAA/Serial # part of the
        mac address.

        Example:
            mac                     : 52:54:58:a5:65:df
            52                      : 01010010
            flip the 7th bit        : 01010010 -> 01010000
            01010000                : 50
            add ff:fe to form eui64 : 5054:58ff:fea5:65df

    """

    ipaddr = validate_prefix(prefix)
    ipaddr = ipaddr.exploded.split(":")
    mac = re.sub("[:-]", "", validate_mac(mac))

    # convert the first octet of the mac-address to binary format
    octet = mac[:2]
    octet_binary = bin(int(octet, 16))[2:].zfill(8)

    # flip the 7th bit of the binary output, e.g. 0->1 1->0
    flipped_bit = str(1 - int(octet_binary[6]))
    octet_binary_flipped = octet_binary[:6] + flipped_bit + octet_binary[7]

    # convert the binary formatted part of the mac address back to hex
    octet_hex_flipped = hex(int(octet_binary_flipped, 2))[2:].zfill(2)

    # rebuild the mac address with the newly flipped bit
    mac_flipped = octet_hex_flipped + mac[2:]

    # construct the eui-64 part of the ip address
    eui64 = (
        mac_flipped[:4]
        + ":"
        + mac_flipped[4:6]
        + "ff:fe"
        + mac_flipped[6:8]
        + ":"
        + mac_flipped[8:]
    )

    # construct the eui-64 address
    eui64addr = ipaddress.ip_address(":".join(ipaddr[:4]) + ":" + eui64)
    return eui64addr


def main(args):
    eui64addr = calculate_eui64addr(args.mac, args.prefix)
    print(eui64addr)


if __name__ == "__main__":
    main(parse_args())
